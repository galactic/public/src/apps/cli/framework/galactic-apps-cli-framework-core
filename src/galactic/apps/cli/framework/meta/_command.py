"""Meta plugin module."""

from __future__ import annotations

import platform
import re
from pathlib import Path
from typing import TYPE_CHECKING, Any, ClassVar

from cleo.helpers import option

from galactic.apps.cli.framework.core import Command as BaseCommand
from galactic.apps.cli.framework.core import CommandHelper
from galactic.apps.cli.main import application

if TYPE_CHECKING:
    from collections.abc import Callable

    from cleo.io.inputs.argument import Argument
    from cleo.io.inputs.option import Option


# pylint: disable=too-many-instance-attributes
class MetaCommand(BaseCommand):
    """
    It represents the command to create a plugin for the framework itself.
    """

    name = f"{BaseCommand.name} self create"
    description = "Create a plugin for the framework itself"
    arguments: ClassVar[list[Argument]] = [
        *BaseCommand.arguments,
    ]
    options: ClassVar[list[Option]] = [
        *BaseCommand.options,
        option(
            long_name="command",
            short_name="c",
            description="Name of the command.",
            flag=False,
            multiple=True,
        ),
    ]

    def __init__(self) -> None:
        super().__init__()
        self.list_commands: list[CommandHelper] = []
        self.command_split_names: list[list[str]] = []
        self.python_files_paths: list[Path] = []
        self.python_files_path_start: Path | None = None
        self.authors: list[str] = []
        self.exit_code = 0

    def handle(self) -> int:
        """
        Handle the command.

        Returns
        -------
        int
            An exit code (0 means no error).

        """
        path = Path(self.argument("path")).resolve()
        self.python_files_path_start = Path(
            path,
            "src",
            "galactic",
            "apps",
            "cli",
            "framework",
        )
        self.exit_code = self._handle_options()

        if self.exit_code == 0:
            self.generate_base_plugin(
                ["meta"],
                self.python_files_paths,
                self.command_split_names,
            )

            templates_and_destinations = {
                "pyproject.toml.j2": Path(path, "pyproject.toml"),
            }
            data = {
                "command_split_names": self.command_split_names,
                "description": self.plugin_description,
                "name": self.plugin_name,
                "python_version": platform.python_version(),
                "authors": self.authors,
            }
            self.generate_files(templates_and_destinations, data)

            for command in self.list_commands:
                python_files_path = self.python_files_path_start
                for word in command.get_split_name():
                    python_files_path = Path(python_files_path, word)
                self.python_files_paths.append(python_files_path)

                templates_and_destinations = {
                    "__init__.py.j2": Path(python_files_path, "__init__.py"),
                    "__init__.pyi.j2": Path(python_files_path, "__init__.pyi"),
                    "_command.py.j2": Path(python_files_path, "_command.py"),
                }

                data = {
                    "command_split_name": command.get_split_name(),
                    "command_description": command.get_description(),
                    "command_class_name": command.get_class_name(),
                    "command_arguments": command.get_arguments(),  # type: ignore[dict-item]
                    "command_options": command.get_options(),  # type: ignore[dict-item]
                }

                self.generate_files(templates_and_destinations, data)

            self.line("<info>Creation of the files... OK.</info>")

        return self.exit_code

    def _handle_options(self) -> int:
        self.plugin_description = "Your plugin generator"
        self.plugin_name = "galactic-framework-plugin"
        self.list_commands = []

        commands_name: list[str] = self.option("command")

        if commands_name:
            first_iteration = True
            for command_name in commands_name:
                self._valid_syntax_command_name(command_name)
                split_name = command_name.split()
                python_files_path = self.python_files_path_start
                for word in split_name:
                    python_files_path = Path(python_files_path, word)  # type: ignore[arg-type]
                self.command_split_names.append(split_name)
                self.python_files_paths.append(python_files_path)  # type: ignore[arg-type]
                command = CommandHelper()
                command.set_split_name(split_name)
                if not first_iteration:
                    self.line("")
                else:
                    first_iteration = False
                self.line(
                    f"<question>Command: "
                    f"<info>galactic framework</info> "
                    f"{command_name}</question>",
                )

                self._handle_function(True, self._handle_command_description, command)
                self._handle_function(True, self._handle_class_name, command)
                self._handle_function(True, self._handle_command_arguments, command)
                self._handle_function(True, self._handle_command_options, command)

                self.list_commands.append(command)

        else:
            stop_iterate = False
            is_interactive = self.io.is_interactive()

            if is_interactive:
                default_value = "n"
                first_iteration = True
                while not stop_iterate:
                    if not first_iteration:
                        question = self.create_question(
                            f"Add another command (y/n) "
                            f"[<comment>{default_value}</comment>]:",
                        )
                        question.set_validator(
                            lambda v: self.validate_confirmation_question(
                                v,
                                default_value,
                            ),
                        )
                        answer = self.ask(question)
                        self.line("")
                    else:
                        answer = True
                        first_iteration = False
                    if answer:
                        command = CommandHelper()

                        self._handle_function(True, self._handle_command_name, command)
                        self._handle_function(
                            True,
                            self._handle_command_description,
                            command,
                        )
                        self._handle_function(True, self._handle_class_name, command)
                        self._handle_function(
                            True,
                            self._handle_command_arguments,
                            command,
                        )
                        self._handle_function(
                            True,
                            self._handle_command_options,
                            command,
                        )

                        self.list_commands.append(command)
                    else:
                        stop_iterate = True

        self.handle_function(True, super().handle_options)

        return self.exit_code

    def _handle_command_name(self, command: CommandHelper) -> int:
        is_interactive = self.io.is_interactive()

        if is_interactive:
            question = self.create_question(
                "Name of the command: <info>galactic framework</info>",
            )
            question.set_validator(self._validate_command_name)
            command_name: str = self.ask(question)

            split_name = command_name.split()
            python_files_path = self.python_files_path_start
            for word in split_name:
                python_files_path = Path(python_files_path, word)  # type: ignore[arg-type]
            self.command_split_names.append(split_name)
            self.python_files_paths.append(python_files_path)  # type: ignore[arg-type]

            command.set_split_name(split_name)

        return 0

    def _handle_command_description(self, command: CommandHelper) -> int:
        is_interactive = self.io.is_interactive()

        if is_interactive:
            default_value = "s"
            question = self.create_question(
                f"Description of the command "
                f"[<comment>{default_value}</comment> to skip]:",
            )
            question.set_validator(
                lambda v: self.validate_open_question(v, default_value),
            )
            description = self.ask(question)

            command.set_description(description)
        return 0

    def _handle_class_name(self, command: CommandHelper) -> int:
        is_interactive = self.io.is_interactive()

        if is_interactive:
            default_value = ""
            for word in command.get_split_name():
                default_value += word[0].upper() + word[1:]
            default_value += "Command"
            question = self.create_question(
                f"Name of the class [<comment>{default_value}</comment>]:",
            )
            question.set_validator(
                lambda v: self._validate_class_name(v, default_value),
            )
            class_name = self.ask(question)

            command.set_class_name(class_name)

        return 0

    def _handle_command_arguments(self, command: CommandHelper) -> int:
        is_interactive = self.io.is_interactive()
        default_value = "n"
        stop_iterate = False

        if is_interactive:
            while not stop_iterate:
                question = self.create_question(
                    f"Add an argument (y/n) [<comment>{default_value}</comment>]:",
                )
                question.set_validator(
                    lambda v: self.validate_confirmation_question(v, default_value),
                )
                answer = self.ask(question)
                if answer:
                    command.add_argument(self._create_argument())
                else:
                    stop_iterate = True

        return 0

    def _handle_command_options(self, command: CommandHelper) -> int:
        is_interactive = self.io.is_interactive()
        default_value = "n"
        stop_iterate = False

        if is_interactive:
            while not stop_iterate:
                question = self.create_question(
                    f"Add an option (y/n) [<comment>{default_value}</comment>]:",
                )
                question.set_validator(
                    lambda v: self.validate_confirmation_question(v, default_value),
                )
                answer = self.ask(question)
                if answer:
                    command.add_option(self._create_option())
                else:
                    stop_iterate = True

        return 0

    def _create_argument(self) -> dict[str, Any]:
        new_argument = {}

        question = self.create_question("  Name:")
        question.set_validator(self._validate_necessary_input_name)
        name = self.ask(question)
        new_argument["name"] = name

        default_value = "s"
        question = self.create_question(
            f"  Description [<comment>{default_value}</comment> to skip]:",
        )
        question.set_validator(lambda v: self.validate_open_question(v, default_value))
        description = self.ask(question)
        if description:
            new_argument["description"] = description

        default_value = "n"
        question = self.create_question(
            f"  Optional (y/n) [<comment>{default_value}</comment>]:",
        )
        question.set_validator(
            lambda v: self.validate_confirmation_question(v, default_value),
        )
        optional = self.ask(question)
        if optional:
            new_argument["optional"] = optional

        default_value = "n"
        question = self.create_question(
            f"  Multiple (y/n) [<comment>{default_value}</comment>]:",
        )
        question.set_validator(
            lambda v: self.validate_confirmation_question(v, default_value),
        )
        multiple = self.ask(question)
        if multiple:
            new_argument["multiple"] = multiple

        if optional:
            self._handle_inputs_default_value(new_argument, multiple)

        return new_argument

    def _create_option(self) -> dict[str, Any]:
        new_option = {}

        question = self.create_question("  Long name:")
        question.set_validator(self._validate_necessary_input_name)
        long_name = self.ask(question)
        new_option["long_name"] = long_name

        default_value = "s"
        question = self.create_question(
            f"  Short name [<comment>{default_value}</comment> to skip]:",
        )
        question.set_validator(
            lambda v: self._validate_option_short_name(v, default_value),
        )
        short_name = self.ask(question)
        if short_name:
            new_option["short_name"] = short_name

        default_value = "s"
        question = self.create_question(
            f"  Description [<comment>{default_value}</comment> to skip]:",
        )
        question.set_validator(lambda v: self.validate_open_question(v, default_value))
        description = self.ask(question)
        if description:
            new_option["description"] = description

        default_value = "y"
        question = self.create_question(
            f"  Flag (y/n) [<comment>{default_value}</comment>]:",
        )
        question.set_validator(
            lambda v: self.validate_confirmation_question(v, default_value),
        )
        flag = self.ask(question)
        if not flag and flag is not None:
            new_option["flag"] = flag

        default_value = "y"
        question = self.create_question(
            f"  Value required (y/n) [<comment>{default_value}</comment>]:",
        )
        question.set_validator(
            lambda v: self.validate_confirmation_question(v, default_value),
        )
        value_required = self.ask(question)
        if not value_required and value_required is not None:
            new_option["value_required"] = value_required

        if not flag:
            default_value = "n"
            question = self.create_question(
                f"  Multiple (y/n) [<comment>{default_value}</comment>]:",
            )
            question.set_validator(
                lambda v: self.validate_confirmation_question(v, default_value),
            )
            multiple = self.ask(question)
            if multiple:
                new_option["multiple"] = multiple

            self._handle_inputs_default_value(new_option, multiple)

        return new_option

    def _handle_inputs_default_value(
        self,
        new_parameter: dict[str, Any],
        multiple: bool,
    ) -> None:
        default_value = "s"
        if multiple:
            stop_iterate = False
            default = []
            while not stop_iterate:
                question = self.create_question(
                    f"  Add a default value "
                    f"[<comment>{default_value}</comment> to skip]:",
                )
                question.set_validator(
                    lambda v: self.validate_open_question(v, default_value),
                )
                answer = self.ask(question)
                if answer is None:
                    stop_iterate = True
                elif answer:
                    default.append(answer)
        else:
            question = self.create_question(
                f"  Default value [<comment>{default_value}</comment> to skip]:",
            )
            question.set_validator(
                lambda v: self.validate_open_question(v, default_value),
            )
            default = self.ask(question)
        if default:
            new_parameter["default"] = default

    @classmethod
    def _validate_command_name(cls, command_name: str) -> str | None:
        if command_name is None:
            raise ValueError(
                "You have to set a name.",
            )

        cls._valid_syntax_command_name(command_name)

        return command_name

    @classmethod
    def _validate_class_name(cls, class_name: str, default: str) -> str | None:
        name = cls.validate_open_question(class_name, default)

        if name:
            cls._valid_syntax_class_name(name)

        return name

    @classmethod
    def _validate_necessary_input_name(cls, argument_name: str) -> str | None:
        if argument_name is None:
            raise ValueError(
                "You have to set a name.",
            )

        cls._valid_syntax_input_name(argument_name)

        return argument_name

    @classmethod
    def _validate_option_short_name(cls, option_name: str, default: str) -> str | None:
        name = cls.validate_open_question(option_name, default)

        if name:
            cls._valid_syntax_short_name(name)

        return option_name

    @classmethod
    def _valid_syntax_command_name(cls, command_name: str) -> None:
        pattern = re.compile(r"^[a-zA-Z0-9_][a-zA-Z0-9\-_ ]*$")
        if not pattern.match(command_name):
            raise ValueError(
                "Invalid syntax. Use only letters, numbers, underscores, hyphens "
                "and spaces.",
            )

    @classmethod
    def _valid_syntax_class_name(cls, class_name: str) -> None:
        pattern = re.compile(r"^[a-zA-Z][a-zA-Z0-9]*$")
        if not pattern.match(class_name):
            raise ValueError(
                "Invalid syntax. Use only letters and numbers. Start with a letter.",
            )

    @classmethod
    def _valid_syntax_input_name(cls, input_name: str) -> None:
        pattern = re.compile(r"^[a-zA-Z][a-zA-Z0-9_\-]*$")
        if not pattern.match(input_name):
            raise ValueError(
                "Invalid syntax. Use only letters, numbers and underscores. "
                "Start with a letter.",
            )

    @classmethod
    def _valid_syntax_short_name(cls, short_name: str) -> None:
        pattern = re.compile(r"^[a-zA-Z]$")
        if not pattern.match(short_name):
            raise ValueError(
                "Invalid syntax. Use only one letter.",
            )

    def _handle_function(
        self,
        condition: bool,
        handle_function: Callable[[CommandHelper], int],
        command: CommandHelper,
    ) -> None:
        if condition and self.exit_code == 0:
            self.exit_code = handle_function(command)


def register() -> None:
    """
    Register the plugin.
    """
    application.add(MetaCommand())
