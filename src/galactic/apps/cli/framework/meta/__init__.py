"""
framework core meta plugin.
"""

from ._command import MetaCommand, register

__all__ = (
    "MetaCommand",
    "register",
)
