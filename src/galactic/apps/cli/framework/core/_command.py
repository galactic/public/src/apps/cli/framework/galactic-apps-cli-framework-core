"""
Framework command module.
"""

from __future__ import annotations

import re
import subprocess
import sys
import unicodedata
from collections.abc import Callable, Mapping
from pathlib import Path
from typing import TYPE_CHECKING, Any, ClassVar

import yaml
from cleo.helpers import argument, option
from jinja2 import Environment, FileSystemLoader
from platformdirs import PlatformDirs

from galactic.apps.cli.main import Command as BaseCommand

if TYPE_CHECKING:
    from cleo.io.inputs.argument import Argument
    from cleo.io.inputs.option import Option


class Command(BaseCommand):  # type: ignore[misc]
    """
    It adds common arguments and options to instances.
    """

    name = "framework"

    AUTHOR_REGEX = re.compile(
        r"(?u)^(?P<name>[- .,\w\d'’\"():&]+)(?: <(?P<email>.+?)>)?$",  # noqa: RUF001
    )

    arguments: ClassVar[list[Argument]] = [
        argument(name="path", description="The path to create the project at."),
    ]

    options: ClassVar[list[Option]] = [
        option(
            long_name="author",
            short_name="a",
            description="Name of an author of the package.",
            flag=False,
            multiple=True,
        ),
        option(
            long_name="no-vcs-author",
            description="Don't use VCS author.",
        ),
        option(
            long_name="description",
            short_name="d",
            description="Description of the package.",
            flag=False,
        ),
        option(
            long_name="name",
            description="Name of the package.",
            flag=False,
        ),
    ]

    def __init__(self) -> None:
        super().__init__()
        self.exit_code: int = 0
        self.authors: list[dict[str, str]] = []
        self.saved_authors = False
        self.plugin_description = ""
        self.plugin_name = ""
        self.env: Environment | None = None

    @classmethod
    def get_personal_config_path(cls, filename: str = "personal_data.yaml") -> Path:
        """
        Get the personnal config path.

        Returns
        -------
        Path
            The personnal config path.
        """
        return Path(
            PlatformDirs("galactic").user_config_path,
            "apps",
            "cli",
            "framework",
            filename,
        )

    def handle_options(self) -> int:
        """
        Handle the options of the command.

        Returns
        -------
        int
            The error code (0 means no error)

        """
        self.saved_authors = False
        authors = self.option("author")
        description = self.option("description")
        name: str = self.option("name")

        if authors:
            self.saved_authors = True
            for author in authors:
                self._valid_syntax_author(author)
            self.authors = self._create_authors(authors)

        if description:
            self.plugin_description = description

        if name:
            self._valid_syntax_name(name)
            self.plugin_name = name

        self.handle_function(not authors, self._handle_authors)
        self.handle_function(not description, self._handle_description)
        self.handle_function(not name, self._handle_name)

        return self.exit_code

    def _handle_authors(self) -> int:
        is_interactive = self.io.is_interactive()

        if is_interactive:
            authors = self._get_author_saved()

            if not authors and not self.option("no-vcs-author"):
                author_git = self._get_author_git_config()
                authors = self._create_authors([author_git]) if author_git != "" else []
            if not authors:
                self.line("<error>You don't have any authors saved.</>")
                self.authors = self._create_authors(self._set_new_authors())
            else:
                self.line("<info>Your saved authors:</>")
                for author in authors:
                    if "email" in author:
                        self.line(
                            f"  - <comment>{author['name']} "
                            f"<{author['email']}></comment>",
                        )
                    else:
                        self.line(f"  - <comment>{author['name']}</comment>")
                default_value = "y"
                question = self.create_question(
                    f"Keep these authors (y/n) "
                    f"[<comment>{default_value}</comment>, s to skip]:",
                )
                question.set_validator(
                    lambda v: self._validate_choice_author(v, default_value),
                )
                answer = self.ask(question, default_value)
                if answer is None:
                    authors = []
                elif not answer:
                    authors = self._create_authors(self._set_new_authors())

            self.authors = authors

        return 0

    @classmethod
    def _create_authors(cls, authors: list[str]) -> list[dict[str, str]]:
        result = []
        for author in authors:
            match = cls.AUTHOR_REGEX.match(author)
            if match:
                if match.group("email"):
                    result.append(
                        {
                            "name": match.group("name"),
                            "email": match.group("email"),
                        },
                    )
                else:
                    result.append(
                        {
                            "name": match.group("name"),
                        },
                    )
        return result

    @classmethod
    def _get_author_saved(cls) -> list[dict[str, str]]:
        path = cls.get_personal_config_path()
        if path.exists():
            with path.open(encoding="utf-8") as file:
                personal_data_file = yaml.safe_load(file)
            if isinstance(personal_data_file, Mapping):
                for index, value in personal_data_file.items():
                    if index == "authors":
                        return value  # type: ignore[no-any-return]
        return []

    @classmethod
    def _get_author_git_config(cls) -> str:
        try:
            name = (
                subprocess.check_output(  # noqa: S603
                    [  # noqa: S607
                        "git",
                        "config",
                        "--global",
                        "--get",
                        "user.name",
                    ],
                )
                .decode()
                .rstrip()
            )
        except (FileNotFoundError, subprocess.CalledProcessError):
            name = ""
        try:
            email = (
                subprocess.check_output(  # noqa: S603
                    [  # noqa: S607
                        "git",
                        "config",
                        "--global",
                        "--get",
                        "user.email",
                    ],
                )
                .decode()
                .rstrip()
            )
        except (FileNotFoundError, subprocess.CalledProcessError):
            email = ""
        if name and email:
            return f"{name} <{email}>"
        return ""

    def _set_new_authors(self) -> list[str]:
        self.saved_authors = True
        authors = []
        stop_iterate = False
        default_value = "s"
        while not stop_iterate:
            question = self.create_question(
                f"Enter an author [<comment>{default_value}</comment> to skip]:",
            )
            question.set_validator(lambda v: self._validate_author(v, default_value))
            author = self.ask(question)
            if author is None:
                stop_iterate = True
            else:
                authors.append(author)

        return authors

    def _handle_description(self) -> int:
        is_interactive = self.io.is_interactive()

        if is_interactive:
            default_value = self.plugin_description
            question = self.create_question(
                f"Description [<comment>{default_value}</comment>, s to skip]:",
            )
            question.set_validator(
                lambda v: self.validate_open_question(v, default_value),
            )
            description = self.ask(question)

            self.plugin_description = description

        return 0

    def _handle_name(self) -> int:
        is_interactive = self.io.is_interactive()

        if is_interactive:
            default_value = self.plugin_name
            question = self.create_question(
                f"Name [<comment>{default_value}</comment>]:",
            )
            question.set_validator(lambda v: self._validate_name(v, default_value))
            name: str = self.ask(question)

            self.plugin_name = name

        return 0

    @classmethod
    def _validate_choice_author(cls, value: str, default: str) -> bool | None:
        value = unicodedata.normalize("NFC", value or default).lower()

        if value in ("yes", "y"):
            return True
        if value in ("no", "n"):
            return False
        if value in ("skip", "s"):
            return None

        raise ValueError(
            "Usage: y => yes; n => no; s => skip",
        )

    @classmethod
    def _validate_author(cls, author: str, default: str) -> str | None:
        res = cls.validate_open_question(author, default)

        if res:
            cls._valid_syntax_author(res)

        return res

    @classmethod
    def _validate_name(cls, name: str, default: str) -> str:
        name = unicodedata.normalize("NFC", name or default).lower()

        cls._valid_syntax_name(name)

        return name

    @classmethod
    def _valid_syntax_author(cls, author: str) -> None:
        match = cls.AUTHOR_REGEX.match(author)
        if not match:
            raise ValueError(
                "Invalid author string. Must be in the format: "
                "John Smith <john@example.com>",
            )

    @classmethod
    def _valid_syntax_name(cls, name: str) -> None:
        pattern = re.compile(r"^[a-zA-Z0-9][a-zA-Z0-9-]*$")
        if not pattern.match(name):
            raise ValueError(
                "Invalid syntax. Use only letters, numbers and hyphens. "
                "Don't start with a hyphen.",
            )

    @classmethod
    def validate_open_question(cls, answer: str, default: str) -> str | None:
        """
        Check the answer of an open question.

        Parameters
        ----------
        answer
            The answer of the question to validate.
        default
            The default answer of the question.

        Returns
        -------
        str | None
            str if the answer is acceptable, None otherwise.

        """
        answer = unicodedata.normalize("NFC", answer or default)

        if answer.lower() in ("s", "skip"):
            return None

        return answer

    @classmethod
    def validate_confirmation_question(cls, answer: str, default: str) -> bool:
        """
        Check the answer of a confirmation question.

        Parameters
        ----------
        answer
            The answer of the question to validate.
        default
            The default answer of the question.

        Returns
        -------
        bool
            True if the answer is acceptable, False otherwise.

        Raises
        ------
        ValueError
            If neither answer nor default is acceptable.

        """
        answer = unicodedata.normalize("NFC", answer or default).lower()

        if answer in ("yes", "y"):
            return True
        if answer in ("no", "n"):
            return False

        raise ValueError(
            "Usage: y => yes; n => no",
        )

    def handle_function(
        self,
        condition: bool,
        handle_function: Callable[[], int],
    ) -> None:
        """
        Factorize the uses of handle_function.

        Parameters
        ----------
        condition
            A condition who determines the execution or not of `handle_function`.
        handle_function
            The handle function to execute.

        """
        if condition and self.exit_code == 0:
            self.exit_code = handle_function()

    def generate_base_plugin(
        self,
        command_name: list[str],
        python_files_paths: list[Path],
        command_split_names: list[list[str]] | None = None,
    ) -> None:
        """
        Generate the base of the plugin.

        Parameters
        ----------
        command_name
            Name of the command who generate the plugin.
        python_files_paths
            List of paths where python files will be.
        command_split_names
            (Optional) Names of the command to create.

        """
        path = Path(self.argument("path")).resolve()

        end_templates_path_base = Path(
            "share",
            "galactic",
            "apps",
            "cli",
            "framework",
            "templates",
        )
        end_templates_path = end_templates_path_base
        for word in command_name:
            end_templates_path = Path(end_templates_path, word)

        templates_path = Path(sys.prefix, end_templates_path)

        # To test the command
        if not templates_path.exists():
            templates_path = Path(
                Path.cwd(),
                end_templates_path,
            )

        self.env = Environment(
            loader=FileSystemLoader(templates_path),
            autoescape=True,
        )
        for python_files_path in python_files_paths:
            Path(python_files_path).mkdir(parents=True, exist_ok=True)
            with Path(python_files_path, "py.typed").open(mode="w+", encoding="utf-8"):
                pass
        Path(path, "tests").mkdir(parents=True, exist_ok=True)
        with Path(path, "README.md").open(mode="w+", encoding="utf-8"):
            pass

        if command_split_names:
            for command_split_name in command_split_names:
                end_templates_path = end_templates_path_base
                for word in command_split_name:
                    end_templates_path = Path(end_templates_path, word)
                Path(path, end_templates_path).mkdir(parents=True, exist_ok=True)

        self.line("<info>Creation of the plugin base... OK.</info>")

    def generate_files(
        self,
        templates_and_destinations: dict[str, Path],
        data: dict[str, Any] | None = None,
    ) -> None:
        """
        Generate files of the plugin.

        Parameters
        ----------
        templates_and_destinations
            A dictionary who stock the name of a template and his destination path.
        data
            A dictionary who stock values to personalize the templates.

        """
        for template_name, dest_template in templates_and_destinations.items():
            template = self.env.get_template(template_name)  # type: ignore[union-attr]
            if data is not None:
                rendered_template = template.render(data)
            else:
                rendered_template = template.render()
            Path(dest_template).write_text(
                rendered_template,
                encoding="utf-8",
            )

    def post(self) -> None:
        """
        Post run execution.
        """
        if self.saved_authors:
            path = self.get_personal_config_path()
            if not path.parent.exists():
                path.parent.mkdir(parents=True, exist_ok=True)
            data = {"authors": self.authors}
            with path.open("w", encoding="utf-8") as stream:
                yaml.dump(data, stream)
            self.line("<info>Authors saved.</info>")
        super().post()
