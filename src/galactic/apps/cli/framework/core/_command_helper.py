"""
Class who stock informations about command.
"""

from __future__ import annotations

from typing import Any


class CommandHelper:
    """
    Store informations about a command.
    """

    split_name: list[str]
    description: str
    class_name: str
    arguments: list[dict[str, Any]]
    options: list[dict[str, Any]]

    def __init__(self) -> None:
        self.split_name: list[str] = []
        self.description: str = ""
        self.class_name: str = ""
        self.arguments: list[dict[str, Any]] = []
        self.options: list[dict[str, Any]] = []

    def get_split_name(self) -> list[str]:
        """
        Getter of `split_name`.

        Returns
        -------
        list[str]
            The name of the command by a list of word.

        """
        return self.split_name

    def get_description(self) -> str:
        """
        Getter of `description`.

        Returns
        -------
        str
            The description of the command.

        """
        return self.description

    def get_class_name(self) -> str:
        """
        Getter of `class_name`.

        Returns
        -------
        str
            The name of the class of the command.

        """
        return self.class_name

    def get_arguments(self) -> list[dict[str, Any]]:
        """
        Getter of `arguments`.

        Returns
        -------
        list[dict[str, Any]]
            The list of arguments of the command.

        """
        return self.arguments

    def get_options(self) -> list[dict[str, Any]]:
        """
        Getter of `options`.

        Returns
        -------
        list[dict[str, Any]]
            The list of options of the command.

        """
        return self.options

    def set_split_name(self, split_name: list[str]) -> None:
        """
        Setter of `split_name`.

        Parameters
        ----------
        split_name
            The name of the command split word by word.

        """
        self.split_name = split_name

    def set_description(self, description: str) -> None:
        """
        Setter of `description`.

        Parameters
        ----------
        description
            The description of the command.

        """
        self.description = description

    def set_class_name(self, class_name: str) -> None:
        """
        Setter of `class_name`.

        Parameters
        ----------
        class_name
            The name of the class who contains the command.

        """
        self.class_name = class_name

    def set_arguments(self, arguments: list[dict[str, Any]]) -> None:
        """
        Setter of `arguments`.

        Parameters
        ----------
        arguments
            The list of arguments of the command.

        """
        self.arguments = arguments

    def set_options(self, options: list[dict[str, Any]]) -> None:
        """
        Setter of `options`.

        Parameters
        ----------
        options
            The list of options of the command.

        """
        self.options = options

    def add_argument(self, argument: dict[str, Any]) -> None:
        """
        Add an argument to the command.

        Parameters
        ----------
        argument
            An argument to add to the command.

        """
        self.arguments.append(argument)

    def add_option(self, option: dict[str, Any]) -> None:
        """
        Add an option to the command.

        Parameters
        ----------
        option
            An option to add to the command.

        """
        self.options.append(option)
