"""Framework test module."""

import subprocess
import sys
from pathlib import Path
from unittest import TestCase

from galactic.apps.cli.framework.core import Command
from galactic.apps.cli.main import application


class FrameworkTestCase(TestCase):
    def setUp(self):
        subprocess.check_call(
            [
                sys.executable,
                "-m",
                "pip",
                "install",
                Path(__file__).parent / "dummy",
            ],
        )
        path = Command.get_personal_config_path()
        if path.exists():
            path.rename(Command.get_personal_config_path("personal_data_tmp.yaml"))

    def tearDown(self):
        subprocess.check_call(
            [
                sys.executable,
                "-m",
                "pip",
                "uninstall",
                "-y",
                "dummy",
            ],
        )
        path = Command.get_personal_config_path()
        if path.exists():
            path.unlink()
        tmp_path = Command.get_personal_config_path("personal_data_tmp.yaml")
        if tmp_path.exists():
            tmp_path.rename(path)

    def test_app(self):
        self.assertEqual(application.name, "galactic")
        galactic = subprocess.run(
            ["galactic", "--no-ansi"],
            capture_output=True,
            text=True,
            check=False,
        )

        galactic_output = "\n".join(galactic.stdout.strip().split("\n")[1:]).strip()
        expected_output = """
Usage:
  command [options] [arguments]

Options:
  -h, --help            Display help for the given command. When no command is given display help for the list command.
  -q, --quiet           Do not output any message.
  -V, --version         Display this application version.
      --ansi            Force ANSI output.
      --no-ansi         Disable ANSI output.
  -n, --no-interaction  Do not ask any interactive question.
  -v|vv|vvv, --verbose  Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug.

Available commands:
  help                    Displays help for a command.
  list                    Lists commands.

 framework
  framework dummy create  Create a dummy plugin
  framework self create   Create a plugin for the framework itself
        """.strip()

        self.maxDiff = None
        self.assertMultiLineEqual(galactic_output, expected_output)

    def test_framework1(self):

        # test 1
        galactic = subprocess.run(
            [
                "galactic",
                "framework",
                "dummy",
                "create",
                "/tmp/dummy",
                "--no-vcs-author",
            ],
            capture_output=True,
            text=True,
            check=True,
            input="John Doe",
        )
        galactic_output = galactic.stdout.strip()
        expected_output = """
Dummy
Creation of the plugin base... OK.
        """.strip()
        self.maxDiff = None
        self.assertMultiLineEqual(galactic_output, expected_output)

    def test_framework3(self):

        # test 5
        galactic = subprocess.run(
            [
                "galactic",
                "framework",
                "dummy",
                "create",
                "/tmp/dummy",
                "--no-ansi",
                "--author",
                "John Doe",
                "--author",
                "John Smith",
                "--description",
                "test",
                "--name",
                "dummy",
            ],
            capture_output=True,
            check=True,
            text=True,
        )

        galactic_output = galactic.stdout.strip()
        expected_output = """
Dummy
Description: test
Name: dummy
Creation of the plugin base... OK.
Authors saved.
        """.strip()
        self.maxDiff = None
        self.assertMultiLineEqual(galactic_output, expected_output)
