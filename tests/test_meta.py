"""Meta test module."""

import subprocess
from importlib.metadata import version
from textwrap import dedent
from unittest import TestCase

from cleo.application import Application
from cleo.testers.command_tester import CommandTester

from galactic.apps.cli.framework.meta import MetaCommand
from galactic.apps.cli.main import application


class FrameworkTestCase(TestCase):
    def test_app(self):
        self.assertEqual(application.name, "galactic")
        galactic = subprocess.run(
            ["galactic", "--no-ansi"],
            capture_output=True,
            text=True,
            check=False,
        )

        galactic_output = galactic.stdout.strip()
        expected_output = f"""
GALACTIC main application (version {version("galactic-apps-cli-main")})

Usage:
  command [options] [arguments]

Options:
  -h, --help            Display help for the given command. When no command is given display help for the list command.
  -q, --quiet           Do not output any message.
  -V, --version         Display this application version.
      --ansi            Force ANSI output.
      --no-ansi         Disable ANSI output.
  -n, --no-interaction  Do not ask any interactive question.
  -v|vv|vvv, --verbose  Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug.

Available commands:
  help                   Displays help for a command.
  list                   Lists commands.

 framework
  framework self create  Create a plugin for the framework itself
        """.strip()

        self.maxDiff = None
        self.assertMultiLineEqual(galactic_output, expected_output)

    def test_meta(self):

        app = Application()
        app.add(MetaCommand())

        command = app.find("framework self create")
        command_tester = CommandTester(command)

        # test 1
        command_tester.execute(
            "/tmp/test --author 'John Doe'",
            inputs=dedent(
                """
                error'
                name command
                
                'error
                
                e
                y
                
                .error
                argument1
                arg1
                y
                y
                v1
                v2
                
                
                y
                option1
                
                opt1
                y
                y
                y
                option2
                
                
                n
                n
                y
                v1
                
                y
                option3
                
                
                n
                
                
                
                
                y
                name command2
                
                
                
                
                
                
                test""",
            ),
        )
        galactic_output = command_tester.io.fetch_output().strip()
        expected_output = """
Creation of the plugin base... OK.
Creation of the files... OK.
Authors saved.
        """.strip()
        galactic_error = command_tester.io.fetch_error().strip()
        expected_error = """
Name of the command: galactic framework You have to set a name.
Name of the command: galactic framework Invalid syntax. Use only letters, numbers, underscores, hyphens and spaces.
Name of the command: galactic framework Description of the command [s to skip]: Name of the class [NameCommandCommand]: Invalid syntax. Use only letters and numbers. Start with a letter.
Name of the class [NameCommandCommand]: Add an argument (y/n) [n]: Usage: y => yes; n => no
Add an argument (y/n) [n]:   Name: You have to set a name.
  Name: Invalid syntax. Use only letters, numbers and underscores. Start with a letter.
  Name:   Description [s to skip]:   Optional (y/n) [n]:   Multiple (y/n) [n]:   Add a default value [s to skip]:   Add a default value [s to skip]:   Add a default value [s to skip]: Add an argument (y/n) [n]: Add an option (y/n) [n]:   Long name:   Short name [s to skip]:   Description [s to skip]:   Flag (y/n) [y]:   Value required (y/n) [y]: Add an option (y/n) [n]:   Long name:   Short name [s to skip]:   Description [s to skip]:   Flag (y/n) [y]:   Value required (y/n) [y]:   Multiple (y/n) [n]:   Add a default value [s to skip]:   Add a default value [s to skip]: Add an option (y/n) [n]:   Long name:   Short name [s to skip]:   Description [s to skip]:   Flag (y/n) [y]:   Value required (y/n) [y]:   Multiple (y/n) [n]:   Default value [s to skip]: Add an option (y/n) [n]: Add another command (y/n) [n]: Name of the command: galactic framework Description of the command [s to skip]: Name of the class [NameCommand2Command]: Add an argument (y/n) [n]: Add an option (y/n) [n]: Add another command (y/n) [n]: Description [Your plugin generator, s to skip]: Name [galactic-framework-plugin]:
        """.strip()
        self.maxDiff = None
        self.assertMultiLineEqual(galactic_output, expected_output)
        self.assertMultiLineEqual(galactic_error, expected_error)

        # test 2
        command_tester.execute(
            "/tmp/test --command command -c command2",
            inputs="\n\n\n\n\n\n\n\n\n\ntest\n",
        )
        galactic_output = command_tester.io.fetch_output().strip()
        expected_output = """
Command: galactic framework command

Command: galactic framework command2
Your saved authors:
  - John Doe
Creation of the plugin base... OK.
Creation of the files... OK.
        """.strip()
        self.assertMultiLineEqual(galactic_output, expected_output)
