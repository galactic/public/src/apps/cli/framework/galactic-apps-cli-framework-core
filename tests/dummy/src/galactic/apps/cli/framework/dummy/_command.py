"""
Dummy module.
"""

from pathlib import Path

from galactic.apps.cli.framework.core import Command
from galactic.apps.cli.main import application


class DummyCommand(Command):
    """
    Dummy command.
    """

    name = f"{Command.name} dummy create"
    description = "Create a dummy plugin"
    options = (*Command.options,)
    arguments = (*Command.arguments,)

    def handle(self) -> int:
        """
        Handle command.

        Returns
        -------
        int
            0
        """
        self.exit_code = self.handle_options()

        if self.exit_code == 0:
            self.line("Dummy")
            if self.plugin_description:
                self.line(f"Description: {self.plugin_description}")
            if self.plugin_name:
                self.line(f"Name: {self.plugin_name}")

            python_file_path_test = Path(self.argument("path"), "galactic")
            self.generate_base_plugin(
                ["meta"],
                [python_file_path_test],
            )  # meta because the templates are in share/.../meta
            self.generate_files(
                {
                    "__init__.py.j2": Path(python_file_path_test, "__init__.py"),
                },
            )

        return self.exit_code

    def _handle_options(self) -> int:
        self.plugin_description = "Test package"
        self.plugin_name = "galactic-framework-test"

        return super().handle_options()


def register() -> None:
    """
    Register plugin.
    """
    if not application.has(DummyCommand.name):
        application.add(DummyCommand())
