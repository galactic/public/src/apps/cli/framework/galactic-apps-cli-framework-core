from ._command import DummyCommand, register

__all__ = ("DummyCommand", "register")
