======================
**GALACTIC** framework
======================

Description
===========

*galactic-apps-cli-framework-core* [#logo]_ [#logoframework]_ is the
framework for creating extensions for **GALACTIC**.

**GALACTIC** stands for
**GA**\ lois
**LA**\ ttices,
**C**\ oncept
**T**\ heory,
**I**\ mplicational systems and
**C**\ losures.

.. list-table:: Research directors
   :class: contributors
   :widths: 100
   :header-rows: 0

   * - |Karell Bertet|

       Karell Bertet (2004-) is a full professor at the L3i.

       She is the principal research director of mathematical
       and computer theories for the **GALACTIC** ecosystem.

       She is at the origin of the first library written in java
       manipulating formal concept analysis and lattices.

.. list-table:: Architects
   :class: contributors
   :widths: 100
   :header-rows: 0

   * - |Christophe Demko|

       Christophe Demko (2011-) is an assistant professor at the L3i.

       He is the software architect of the :mod:`galactic` python package.

.. list-table:: Lead developers
   :class: contributors
   :widths: 100
   :header-rows: 0

   * - |Alexy Lafosse|

       `Alexy Lafosse <https://www.linkedin.com/in/alexy-lafosse-686623255/>`_
       (2024) completed his computer science undergraduate
       internship at the L3i in 2024.

       He delivered the first functional
       version of the **GALACTIC** framework and its initial plugins,
       including the meta plugin, which allows for the creation of
       extensions for the framework itself, and the
       `plugin for creating Python packages for data input-output <https://galactic.univ-lr.fr/docs/apps/cli/framework/io/data/>`_.

..  toctree::
    :maxdepth: 1
    :caption: Getting started

    install/index.rst

..  toctree::
    :maxdepth: 1
    :caption: Release Notes

    release-notes.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. [#logo]
        The **GALACTIC** icon has been constructed using icons present in the
        `free star wars icons set <http://sensibleworld.com/news/free-star-wars-icons>`_
        designed by `Sensible World <http://www.iconarchive.com/artist/sensibleworld.html>`_.
        The product or characters depicted in these icons
        are © by Disney / Lucasfilm.

        It's a tribute to the french mathematician
        `Évariste Galois <https://en.wikipedia.org/wiki/Évariste_Galois>`_ who
        died at age 20 in a duel.
        In France, Concept Lattices are also called *Galois Lattices*.

.. [#logoframework]
        The *galactic-apps-cli-framework-core* icon has been constructed using the main
        **GALACTIC** icon and the
        `Ambience, context, fix icon <https://www.iconfinder.com/icons/3018534/ambience_context_fix_framework_options_set_settings_icon>`_
        designed by
        `PINPOINT.WORLD <https://www.iconfinder.com/pinpointworld>`_

.. |Karell Bertet| image:: https://www.gravatar.com/avatar/8cb290d97e10379a9385b2bec6d4429741c58c37d77d6318906b359730ff03a5?size=100
.. |Christophe Demko| image:: https://www.gravatar.com/avatar/4ddabe5dc55cc11f1759600e15f95eaf60f2a4b8d0970a0a268e873d9a9321ce?size=100
.. |Alexy Lafosse| image:: https://www.gravatar.com/avatar/141ef57578e7d9a9ac55b2fad76c4b38c226ad1177415bb142a24eea3c62f2f8?size=100

